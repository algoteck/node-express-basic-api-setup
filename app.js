const express = require("express");
const app = express();

const port = 8000;

app.get("/", (res, req) => {
  return res.statusCode(200).send({
    success: true,
    message: "API Basic Route",
  });
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
