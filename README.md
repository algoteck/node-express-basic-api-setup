# node-express-basic-api-setup

In this example application we will setup basic express api application.

## Running the Server

With [NodeJS](https://nodejs.org/en/) installed, you can started the server by running,

```sh
node app.js
```

_**OR**_

```sh
npm run start
```

## Development

This simple server can be easily extended. After cloning this [repository](https://bitbucket.org/algoteck/node-express-basic-api-setup) you can start developing locally.

### Locally

1. Install [Nodemon](https://www.npmjs.com/package/nodemon), Nodemon will watch for file changes and restart the NodeJS process. This allows for faster development and testing.

```sh
npm install -g nodemon
```

2. With Nodemon installed, start the server using Nodemon

```sh
nodemon app.js
```

_**OR**_

```sh
npm run start:dev
```
